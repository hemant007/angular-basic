import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  constructor(private modalService: BsModalService) { }
      model: any = {};
     
      fileToUpload: File = null;
      public modalRef: BsModalRef;
      
    
      onSubmit() {
        alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.model))
      }

      public openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template); // {3}
      }

  
  ngOnInit() {
    console.log('this ==>', this);
  }

}
